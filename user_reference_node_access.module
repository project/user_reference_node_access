<?php

/**
 * Restricts node access to users referenced by a user reference field.
 */

use Drupal\node\NodeInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_node_access_records().
 *
 * @inheritdoc
 */
function user_reference_node_access_node_access_records(NodeInterface $node) {

  // Get our form config values.
  $config = \Drupal::config('user_reference_node_access.user_reference_node_access_settings');

  // Get the user reference field name.
  $userReferenceFieldName = $config->get('user_reference_field_name');

  // Check if user reference field exists.
  if ($node->hasField($userReferenceFieldName)) {

    // Get referenced users.
    $referenced_users = $node->get($userReferenceFieldName)->getValue();

    // Instantiate $grants array.
    $grants = [];

    // Ensure node is published.
    if ($node->isPublished()) {

      // Loop through our referenced users.
      foreach ($referenced_users as $referenced_user) {

        // Create access grants for referenced users.
        $grants[] = [
          'realm' => 'user_reference_node_access',
          'gid' => $referenced_user['target_id'],
          'grant_view' => $config->get('user_reference_grants_view') ?: 1,
          'grant_update' => $config->get('user_reference_grants_edit') ?: 0,
          'grant_delete' => $config->get('user_reference_grants_delete') ?: 0,
          'langcode' => $node->langcode->value,
        ];

      }

    }

    // Check we can get owner.
    if ($node->getOwnerId()) {

      // Create access grants for node owner.
      $grants[] = [
        'realm' => 'user_reference_node_access_owner',
        'gid' => $node->getOwnerId(),
        'grant_view' => $config->get('node_author_grants_view') ?: 1,
        'grant_update' => $config->get('node_author_grants_edit') ?: 1,
        'grant_delete' => $config->get('node_author_grants_delete') ?: 1,
        'langcode' => $node->langcode->value,
      ];

    }

    // Create "full" access grants for administrators.
    $grants[] = [
      'realm' => 'user_reference_node_access_admin',
      'gid' => 0,
      'grant_view' => 1,
      'grant_update' => 1,
      'grant_delete' => 1,
      'langcode' => $node->langcode->value,
    ];

    return $grants;

  }

}

/**
 * Implements hook_node_grants().
 *
 * @inheritdoc
 */
function user_reference_node_access_node_grants(AccountInterface $account, $op) {

  $grants = [];

  // Access to referenced users.
  $grants['user_reference_node_access'] = [
    $account->id(),
  ];

  // Access to node owner.
  $grants['user_reference_node_access_owner'] = [
    $account->id(),
  ];

  // Always give administrator full access.
  if (in_array('administrator', $account->getRoles())) {

    // Gid to view, edit, delete.
    $grants['user_reference_node_access_admin'][] = 0;
    return $grants;

  }

  return $grants;

}

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function user_reference_node_access_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.user_reference_node_access':
      $text = file_get_contents(dirname(__FILE__) . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}
