INTRODUCTION
------------

# User Reference Node Access

## Description
This module restricts node access to users referenced through a user reference field.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/user_reference_node_access

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/user_reference_node_access


REQUIREMENTS
------------

This module requires the following modules:

 * field
 * node
 * User


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
   
1. **Install** this module
2. Adjust the available settings at: 
   /admin/config/tinsel-suite/user-reference-node-access


CONFIGURATION
------------

 * Customize the settings in Administration » Configuration » 
 Tinsel Suite » User Reference Node Access Settings.


MAINTAINERS
-----------

Current maintainers:
 * Preston Schmidt - https://www.drupal.org/user/3594865
